<?php

$string['formatproglist'] = 'Programming Practice List';
$string['nameproglist'] = 'List';
$string['programminglist'] = 'Programming Practice List';
$string['addprogrammingpractice'] = 'Add programming practice';
$string['id'] = 'Global ID';
$string['name'] = 'Name';
$string['submited'] = 'Submit Count';
$string['accepted'] = 'Accept Count';
$string['ratio'] = 'Ratio';
$string['date'] = 'Date';

?>
