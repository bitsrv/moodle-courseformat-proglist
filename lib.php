<?php

include_once('../lib/tablelib.php');

function print_programming_list($course)
{
    global $CFG;

    $sql = "SELECT p.*, psc.sc AS submited, pac.ac AS accepted,
                   pac.ac / psc.sc AS ratio
              FROM (
            SELECT id, name, globalid, timemodified
              FROM {$CFG->prefix}programming
             WHERE course={$course->id}) AS p
         LEFT JOIN (
            SELECT programmingid, COUNT(*) AS sc
              FROM {$CFG->prefix}programming_submits
          GROUP BY programmingid) AS psc
                ON (p.id = psc.programmingid)
         LEFT JOIN (
            SELECT programmingid, COUNT(*) AS ac
              FROM {$CFG->prefix}programming_submits
             WHERE passed = 1
          GROUP BY programmingid) AS pac
                ON (p.id = pac.programmingid)
          ORDER BY globalid";
    $problems = get_records_sql($sql);

    if (!$problems) {
    	return;
    }

    $table = new flexible_table('format-programming-list');
  
    $table->define_columns(array(
        'id', 'name', 'submited', 'accepted', 'ratio', 'date'));
    $table->define_headers(array(
        get_string('id', 'format_proglist'),
        get_string('name', 'format_proglist'),
        get_string('submited', 'format_proglist'),
        get_string('accepted', 'format_proglist'),
        get_string('ratio', 'format_proglist'),
        get_string('date', 'format_proglist'),
    ));
    $table->set_attribute('cellspacing', '1');
    $table->set_attribute('cellpadding', '3');
    $table->set_attribute('width', '100%');
    $table->set_attribute('id', 'programming-latest-ac');
    $table->set_attribute('class', 'generaltable generalbox');
    $table->set_attribute('align', 'center');
    $table->setup();

    foreach($problems as $programming) {
        $table->add_data(array(
            $programming->globalid,
            '<a href="../mod/programming/view.php?a='.$programming->id.'">'.$programming->name.'</a>',
            $programming->submited,
            $programming->accepted,
            sprintf('%5.2f%%', $programming->ratio * 100),
            userdate($programming->timemodified, '%Y-%m-%d')
        ));
    };

    $table->print_html();
}

function print_add_programming_link($course)
{
  echo '<a href="mod.php?id='.$course->id.'&amp;section=1&amp;add=programming&amp;sesskey='.sesskey().'">'.get_string('addprogrammingpractice', 'format_proglist').'</a>';
}

?>
